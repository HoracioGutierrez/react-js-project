import { model, Model, prop, modelFlow, _async, _await } from "mobx-keystone";
import { Image } from "./Image";

@model("Album")
export default class Album extends Model({
  authorList: prop<string[]>(() => []),
  imageList: prop<Image[]>(() => []),
}) {
  pageAuthor: number = 1;
  pageImages: number = 1;
  @modelFlow
  fetchAuthor = _async(function* (this: Album) {
    const data = yield* _await(
      fetch(`https://picsum.photos/v2/list?page=${this.pageAuthor}&limit=500`)
    );
    const jsonImages = yield* _await(data.json());

    if (jsonImages) {
      this.pageAuthor++;
      jsonImages.forEach((json: Image) => {
        const author = this.authorList.find((item) => json.author === item);
        if (!author) {
          this.authorList.push(json.author);
        }
      });
    }
  });

  @modelFlow
  fetchImages = _async(function* (this: Album) {
    const data = yield* _await(
      fetch(`https://picsum.photos/v2/list?page=${this.pageImages}&limit=500`)
    );
    const jsonImages = yield* _await(data.json());

    if (jsonImages) {
      this.pageImages++;
      jsonImages.forEach((json: Image) => {
        const image = this.imageList.find((item) => json.id === item.id);
        if (!image) {
          this.imageList.push(new Image(json));
        }
      });
    }
  });
}
