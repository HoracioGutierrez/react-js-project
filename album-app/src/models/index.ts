import { registerRootStore } from "mobx-keystone";
import React from "react";
import RootStore from "./Album";

const StoreContext = React.createContext<RootStore>({} as RootStore);

const useStore = () => React.useContext(StoreContext);
const { Provider: StoreProvider } = StoreContext;

function createRootStore() {
  const rootStore = new RootStore({});
  registerRootStore(rootStore);

  return rootStore;
}

export { createRootStore, useStore, StoreProvider, StoreContext, RootStore };
