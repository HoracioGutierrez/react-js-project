import "mobx-react-lite/batchingForReactDom";
import React from "react";
import Masonry from "react-masonry-css";
import { observer } from "mobx-react-lite";
import { Link } from "react-router-dom";
import { useStore } from "../models";
import { Image } from "../models/Image";
import AlbumNav from "./AlbumNav";
import "../css/Album.css";

const AlbumAllAuthor = () => {
  const album = useStore();

  return (
    <div>
      <AlbumNav />
      <div>
        <Masonry
          breakpointCols={3}
          className="my-masonry-grid"
          columnClassName="my-masonry-grid_column"
        >
          {album.imageList.map((imageItem: Image) => (
            <Link key={imageItem.id} to={`/author/all/${imageItem.id}`}>
              <img
                className="image"
                src={`https://picsum.photos/id/${imageItem.id}/${imageItem.width}/${imageItem.height}`}
                alt={imageItem.id}
              />
            </Link>
          ))}
        </Masonry>
      </div>
      <button className="block" onClick={() => album.fetchImages()}>
        LOAD MORE
      </button>
    </div>
  );
};

export default observer(AlbumAllAuthor);
