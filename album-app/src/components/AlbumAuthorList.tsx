import "mobx-react-lite/batchingForReactDom";
import React, { useState, useEffect } from "react";
import { observer } from "mobx-react-lite";
import { useStore } from "../models";
import { Link } from "react-router-dom";
import AlbumNav from "./AlbumNav";
import avatar from "../image/avatar.jpg";
import "../css/AuthorList.css";

const AlbumAuthorList = () => {
  const album = useStore();

  const sortAuthorListAlphabetical = () => {
    let sortedList = [];
    sortedList = album.authorList.slice().sort(function (a, b) {
      if (a < b) return -1;
      else if (a > b) return 1;
      return 0;
    });

    return sortedList;
  };

  const [search, setSearch]: [string, (search: string) => void] = useState("");
  const [authorList, setAuthorList] = useState(sortAuthorListAlphabetical());

  const handleChange = (e: { target: { value: string } }) => {
    setSearch(e.target.value);
  };

  useEffect(() => {}, [search, authorList]);

  return (
    <div>
      <AlbumNav />
      <div className="container">
        <div className="side">
          <input
            type="text"
            onChange={handleChange}
            placeholder="Search for Photographer"
          />
          <ul className="authorListFiltered">
            {authorList.map((author: string) => {
              if (
                search === "" ||
                author.toLowerCase().includes(search.toLowerCase())
              ) {
                return (
                  <li key={author}>
                    <Link key={author} to={`/author/${author}`}>
                      <p>{author}</p>
                    </Link>
                  </li>
                );
              }
              return null;
            })}
          </ul>
        </div>
        <div className="content">
          {sortAuthorListAlphabetical().map((imageAuthor: string) => (
            <div className="card">
              <img className="avatar" src={avatar} alt="Avatar"></img>
              <Link key={imageAuthor} to={`/author/${imageAuthor}`}>
                <p> {imageAuthor} </p>
              </Link>
            </div>
          ))}
          <button
            className="loadAuthorListBtn"
            onClick={() => album.fetchAuthor()}
          >
            LOAD PHOTOGRAPHERS LIST
          </button>
        </div>
      </div>
    </div>
  );
};

export default observer(AlbumAuthorList);
