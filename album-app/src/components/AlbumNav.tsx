import "mobx-react-lite/batchingForReactDom";
import React from "react";
import { Link } from "react-router-dom";
import { observer } from "mobx-react-lite";
import "../css/Nav.css";
import logo from "../image/logo.png";

const AlbumNav = () => {
  return (
    <div className="navbar">
      <Link to="/">
        <img className="logo" src={logo} alt="logo"></img>
      </Link>
      <Link to="/author/all">
        <p>IMAGES</p>
      </Link>
      <Link to="/author/list">
        <p>AUTHOR</p>
      </Link>
    </div>
  );
};

export default observer(AlbumNav);
