/* eslint-disable @typescript-eslint/no-unused-expressions */
import React, { useContext, useState, useEffect } from "react";
import { observer } from "mobx-react-lite";

import { Item } from "../models/Item";
import Store from "../models/List";
import ToDoDelete from "./ToDoDelete";
import ToDoEdit from "./ToDoEdit";

interface TodoItemProp {
  item: Item;
  isChange: boolean;
  setIsChange: Function;
}

interface ItemProp {
  item: Item;
  isChange: boolean;
  setIsEdit: Function;
  setIsDelete: Function;
  setIsDone: Function;
  isDone: boolean;
}

interface TodoItemButtomProps {
  setIsEdit: Function;
  setIsDelete: Function;
}

const ItemButtons: React.FC<TodoItemButtomProps> = ({
  setIsEdit,
  setIsDelete,
}) => {
  return (
    <div>
      <button className="editBtn" onClick={() => setIsEdit(true)}>
        Edit
      </button>
      <button className="deleteBtn" onClick={() => setIsDelete(true)}>
        Delete
      </button>
    </div>
  );
};

const ItemTodo: React.FC<ItemProp> = ({
  item,
  isChange,
  setIsEdit,
  setIsDelete,
  setIsDone,
  isDone,
}) => {
  const store = useContext(Store);
  const handleMarkDone = () => {
    console.log("check mark");
    store.markDoneItem(item.id);
    setIsDone(!isDone);
  };
  return (
    <div>
      <form onSubmit={handleMarkDone}>
        <input
          type="checkbox"
          checked={item.isDone}
          onChange={handleMarkDone}
        />
        <p
          style={
            item.isDone
              ? { textDecoration: "line-through" }
              : { textDecoration: "none" }
          }
        >
          {item.task}
        </p>
      </form>
      <ItemButtons setIsEdit={setIsEdit} setIsDelete={setIsDelete} />
    </div>
  );
};

const TodoItem: React.FC<TodoItemProp> = ({ item, isChange, setIsChange }) => {
  const [isEdit, setIsEdit] = useState(false);
  const [isDelete, setIsDelete] = useState(false);
  const [isDone, setIsDone] = useState(false);

  useEffect(
    () => setIsChange(isEdit || isDelete || isDone),
    [setIsChange, isEdit, isDelete, isDone]
  );

  return (
    <div className="todoListItem">
      {isDelete ? (
        <ToDoDelete item={item} setIsDelete={setIsDelete} />
      ) : isEdit ? (
        <ToDoEdit item={item} setIsEdit={setIsEdit} />
      ) : (
        <ItemTodo
          item={item}
          isChange={isChange}
          setIsEdit={setIsEdit}
          setIsDelete={setIsDelete}
          setIsDone={setIsDone}
          isDone={isDone}
        />
      )}
    </div>
  );
};

export default observer(TodoItem);
