import React, { useContext, useState } from "react";
import Store from "../models/List";
import { observer } from "mobx-react-lite";
import TodoItem from "./TodoItem";

const TodoList = () => {
  const store = useContext(Store);
  const [isChange, setIsChange] = useState(false);

  return (
    <div className="todoList">
      {store.itemList.map((item, index) => (
        <TodoItem
          item={item}
          key={item.id}
          isChange={isChange}
          setIsChange={setIsChange}
        />
      ))}
    </div>
  );
};

export default observer(TodoList);
