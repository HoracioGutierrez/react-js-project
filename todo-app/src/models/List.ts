import { model, Model, prop, modelAction } from "mobx-keystone";
import { createContext } from "react";
import { Item } from "./Item";

@model("List")
export class List extends Model({
  itemList: prop<Item[]>(() => []),
}) {
  @modelAction
  addItem(item: Item) {
    this.itemList.push(item);
  }
  @modelAction
  deleteItem(deletedId: string) {
    this.itemList = this.itemList.filter((item) => {
      return item.id !== deletedId;
    });
  }
  @modelAction
  editItem(selectedId: string, editedItem: string) {
    this.itemList.forEach((item) => {
      if (item.id === selectedId) item.edit(editedItem);
    });
  }
  @modelAction
  markDoneItem(selectedId: string) {
    console.log("AM I BEING CALLED?");
    this.itemList.forEach((item) => {
      if (item.id === selectedId) item.markDone();
    });
  }
}

export default createContext(new List({}));
